<?php

namespace App\Http\Controllers;

use App\User;
use App\Usuario_Rol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Caffeinated\Shinobi\Models\Role;
use DB;
class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
        $query=trim($request->get('searchText'));
        $usuarios=DB::table('users')
        ->orderBy('id', 'desc')
        ->paginate(7);
        return view('usuarios.index',["usuarios"=>$usuarios,"searchText"=>$query]);
        }
    }

    
    public function create()
    {

         $perfiles=DB::table('roles')->get();
            return view("usuarios.create",["perfiles"=>$perfiles]);
    
         
    }

    
    public function store(Request $request)
    {
       $usuario=new User;
        $usuario->name=$request->get('name');
        $usuario->email=$request->get('email');
        $usuario->password= Hash::make($request->get('password'));
        $usuario->save();
        $rol_usu=new Usuario_Rol;
        $rol_usu->role_id=$request->get('perfil');
         $rol_usu->user_id=$usuario->id;
         $rol_usu->save();
         return redirect('usuarios')->with('info','Usuario Guardado con exito');
    }

    
    public function show(User $user)
    {
        //
    }

    
    public function edit($id)
    {
        //$roles=Role::get();
       return view("usuarios.edit",["usuario"=>User::findOrFail($id),"role"=>Role::findOrFail($id)]);
    }

    
    public function update(Request $request,$id)
    {
        $usuario=User::findOrFail($id);
        $usuario->name=$request->get('name');
        $usuario->email=$request->get('email');
        $usuario->update();
        $usuario->roles()->sync($request->get('roles'));
         return redirect('usuarios')->with('info','Usuario Editado con exito');
    }

    
    public function destroy($id)
    {
         $usuario=User::findOrFail($id);
      $usuario->estado='Inactivo';
        $usuario->update();
        return redirect('usuarios');
    }
}
