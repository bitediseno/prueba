<?php

namespace App\Http\Controllers;
use App\Venta;
use App\DetalleVenta;
use Illuminate\Http\Request;
use App\Http\Requests\VentaFormRequest;
use Codedge\Fpdf\Facades\Fpdf;
use DB;
use Carbon\Carbon;
class VentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $query=trim($request->get('searchText'));
            $ventas=DB::table('ventas as ve')
            ->join('detalle_ventas as dtv','ve.cod_venta','=','dtv.cod_venta')
            ->join('clientes as pr','pr.cod_cliente','=','ve.cod_cliente')
            ->join('tipo_venta as tv','tv.cod_tipo_venta','=','ve.cod_tipo_venta')
            ->select('ve.cod_venta','ve.fecha_venta','tv.desc_tipo_venta','ve.numero_factura','ve.total_venta','ve.estado','pr.nombres',DB::raw('sum(dtv.cantidad) as cantidad'))
            ->where('ve.numero_factura','LIKE','%'.$query.'%')
            ->orderBy('ve.cod_venta','desc')
            ->groupBy('ve.cod_venta','ve.fecha_venta','tv.desc_tipo_venta','ve.numero_factura','ve.total_venta','ve.estado','pr.nombres')
            ->paginate(7);
            return view('ventas.index',["ventas"=>$ventas,"searchText"=>$query]);
    }

    public function create()
    {
        $clientes=DB::table('clientes')->where('cod_tipo_cliente','=','1')->get();
        $tipo_ventas=DB::table('tipo_venta')->get();
        $productos=DB::table('productos as prod')
            ->select(DB::raw('CONCAT(prod.codigo," ",prod.nombre_producto)as producto'),'prod.cod_producto','prod.precio_venta','prod.precio_compra')
            ->where ('prod.estado','=','Activo')
             ->where ('prod.cantidad','>=',1)
            ->get();
            return view("ventas.create",["clientes"=>$clientes,"tipo_ventas"=>$tipo_ventas,"productos"=>$productos]);
    }

     public function store(VentaFormRequest $request)
    {
         try{
         DB::beginTransaction();
         $venta=new Venta;
            $venta->cod_cliente=$request->get('cod_cliente');
            $venta->cod_tipo_venta=$request->get('cod_tipo_venta');
           // $venta->fecha_venta=$request->get('fecha_compra');
            $venta->numero_factura=$request->get('numero_factura');
            $venta->total_venta=$request->get('total_venta');
            $venta->estado='Activo';

            $venta->save();
             $cod_producto=$request->get('idarticulo');
            $cantidad=$request->get('cantidad');
            $precio_venta=$request->get('precio_venta');
            $descuento=$request->get('descuento');
//$subtotal=$request->get('total_sub');
          //  echo $subtotal;
            $cont=0;
            while ($cont<count($cod_producto)){
                $detalle=new DetalleVenta();
                $detalle->cod_venta=$venta->cod_venta;
                $detalle->cod_producto=$cod_producto[$cont];
                $detalle->cantidad=$cantidad[$cont];
                $detalle->precio_venta=$precio_venta[$cont];
                $detalle->descuento=$descuento[$cont];
                $detalle->subtotal=$precio_venta[$cont]*$cantidad[$cont];
                $detalle->save();
                $cont=$cont+1;

            }
            DB::Commit();
        }catch(\Exception $e)
       {
             DB::rollback();
        }
        return Redirect('ventas')->with('info','Venta Realizada y  Guardada con exito');
    }
 public function destroy($id)
    {
        $venta=Venta::findOrFail($id);
        $venta->Estado='Anulado';
        $venta->update();
        return Redirect('ventas')->with('info','Venta Anulada con exito');
    }
/*function export_pdf($id) 
  { 
    //dd();
    // Recuperar todos los clientes de la base de datos 
    $data=DB::table('ventas as ve')
            ->join('clientes as pr','pr.cod_cliente','=','ve.cod_cliente')
            ->join('tipo_venta as tv','tv.cod_tipo_venta','=','ve.cod_tipo_venta')
            ->select('ve.cod_venta','ve.fecha_venta','tv.desc_tipo_venta','ve.numero_factura','ve.total_venta','ve.estado','pr.nombres','pr.ruc')
            ->where('ve.cod_venta','=',$id)
            ->orderBy('ve.cod_venta','desc')
            ->get(); 
    $detalle_venta=DB::table('detalle_ventas as dtv')
            ->join('productos as p','p.cod_producto','=','dtv.cod_producto')
            ->select('dtv.cod_venta','p.codigo','p.nombre_producto',DB::raw('sum(dtv.cantidad) as cantidad'),'dtv.precio_venta','dtv.descuento')
            ->where('dtv.cod_venta','=',$id)
            ->orderBy('dtv.cod_venta','desc')
            ->groupBy('dtv.cod_venta','p.codigo','p.nombre_producto','dtv.precio_venta','dtv.descuento')
            ->get(); 
    
    // Enviar datos a la vista utilizando la función loadView de la fachada PDF 

    $pdf=\PDF::loadView('ventas.pdf.factura', compact('data','detalle_venta')); 
    // Si desea almacenar el pdf generado en el servidor, puede usar la función de almacenamiento 
    //$pdf->save(storage_path().'_filename.pdf '); 
    // Finalmente, puede descargar el archivo usando la función de descarga 
    return  $pdf->download('factura.pdf'); 
    
  } */
  function export_pdf($id)
  {
    //buscamos la cabecera
    $ventas=DB::table('ventas as ve')
            ->join('clientes as pr','pr.cod_cliente','=','ve.cod_cliente')
            ->join('tipo_venta as tv','tv.cod_tipo_venta','=','ve.cod_tipo_venta')
            ->select('ve.cod_venta','ve.fecha_venta','tv.desc_tipo_venta','ve.numero_factura','ve.total_venta','ve.estado','pr.nombres','pr.ruc')
            ->where('ve.cod_venta','=',$id)
            ->orderBy('ve.cod_venta','desc')
            ->first(); 
      $mytime=Carbon::now('America/Asuncion');       
    ob_end_clean(); 
    
    $public_path=public_path();
    Fpdf::AddPage();
    Fpdf::Image($public_path.'/imagenes/factura/factura.png',0,0,210,150);
  
    //Fecha de emision
    $fecha_venta=$ventas->fecha_venta=$mytime->toDateString();
     Fpdf::SetXY(35,34);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,$fecha_venta,0,'L');

       Fpdf::SetXY(138,34);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,$fecha_venta,0,'L');
     //condicion de venta
     $condicion_venta=$ventas->desc_tipo_venta;
     if ($condicion_venta=='Contado'){
        Fpdf::SetXY(84,34);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,'x',0,'L');

     Fpdf::SetXY(185,34);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,'x',0,'L');

     }
     if ($condicion_venta=='Credito'){
         Fpdf::SetXY(192,34);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,'x',0,'L');
        
     }
     //razon social
     $razon_social=$ventas->nombres;
     Fpdf::SetXY(43,37);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,$razon_social,0,'L');
      
      Fpdf::SetXY(145,37);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,$razon_social,0,'L');  

     //ruc
       Fpdf::SetXY(20,41);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,$ventas->ruc,0,'L');

         Fpdf::SetXY(120,41);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(25,5,$ventas->ruc,0,'L');

     //direccion
     Fpdf::SetXY(25,45);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,'Obispo Maiz 2585',0,'L');

      Fpdf::SetXY(130,45);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,'Obispo Maiz 2585',0,'L');

///detalle de factura
     $detalleventas=DB::table('detalle_ventas as dv')
            ->join('productos as p','p.cod_producto','=','dv.cod_producto')
            ->select('dv.cod_det_venta','dv.cod_venta','dv.cod_producto','dv.cantidad','dv.precio_venta','dv.subtotal',
'p.nombre_producto','dv.subtotal','dv.descuento')
            ->where('dv.cod_venta','=',$id)
            ->get(); 
            $conta=0;
            $total=0;
       foreach ($detalleventas as $detalleventa) {
           # code...
           //cantidad
        Fpdf::SetXY(12,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->cantidad,0,'L');

      Fpdf::SetXY(112,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->cantidad,0,'L');

     //descripcion
         Fpdf::SetXY(20,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->nombre_producto,0,'L');

          Fpdf::SetXY(122,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->nombre_producto,0,'L');

     //precio unitario
      Fpdf::SetXY(65,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->precio_venta,0,'L');

     Fpdf::SetXY(165,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->precio_venta,0,'L');

      //subtotal
      Fpdf::SetXY(84,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->subtotal,0,'L');

      Fpdf::SetXY(190,60+$conta);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,$detalleventa->subtotal,0,'L');

     $conta=$conta +5;
     $total=$total+$detalleventa->subtotal;
       }   

       //subtotales
         Fpdf::SetXY(84,125);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5, $total,0,'L');

          Fpdf::SetXY(184,125);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5, $total,0,'L');

      //Total
         Fpdf::SetXY(84,130);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5, $total,0,'L');

        Fpdf::SetXY(180,130);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5, $total,0,'L');
         //Total iva 10
         Fpdf::SetXY(50,135);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5, round($total / 11,0),0,'L');

        Fpdf::SetXY(150,135);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5, round($total / 11,0),0,'L');
       //Total iva
         Fpdf::SetXY(76,135);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,round($total / 11,0),0,'L');

         Fpdf::SetXY(176,135);
    Fpdf::SetTextColor(0,30,0);
     Fpdf::SetFont('Times','',9);
     Fpdf::MultiCell(76,5,round($total / 11,0),0,'L');

     Fpdf::Output();

  }

  
}