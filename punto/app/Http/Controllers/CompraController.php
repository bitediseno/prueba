<?php

namespace App\Http\Controllers;

use App\Compra;
use App\DetalleCompra;
use Illuminate\Http\Request;
use App\Http\Requests\CompraFormRequest;
use DB;
class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $query=trim($request->get('searchText'));
            $compras=DB::table('compras as co')
            ->join('detalle_compras as dtc','co.cod_compra','=','dtc.cod_compra')
            ->join('clientes as pr','pr.cod_cliente','=','co.cod_proveedor')
            ->join('tipo_comprobante as tc','tc.cod_tipo_comprobante','=','co.cod_tipo_comprobante')
            ->select('co.cod_compra','co.fecha_compra','dtc.precio_compra','tc.desc_tipo_comprobante','co.numero_comprobante','co.total_compra','co.estado',DB::raw('sum(dtc.cantidad) as cantidad'))
            ->where('co.numero_comprobante','LIKE','%'.$query.'%')
            ->orderBy('co.cod_compra','desc')
            ->groupBy('co.cod_compra','co.fecha_compra','dtc.precio_compra','tc.desc_tipo_comprobante','co.numero_comprobante','co.total_compra','co.estado')
            ->paginate(7);
            return view('compras.index',["compras"=>$compras,"searchText"=>$query]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores=DB::table('clientes')->where('cod_tipo_cliente','=','2')->get();
        $tipo_comprobantes=DB::table('tipo_comprobante')->get();
        $productos=DB::table('productos as prod')
            ->select(DB::raw('CONCAT(prod.codigo," ",prod.nombre_producto)as producto'),'prod.cod_producto','prod.precio_venta','prod.precio_compra')
            ->where ('prod.estado','=','Activo')
            ->get();
            return view("compras.create",["proveedores"=>$proveedores,"tipo_comprobantes"=>$tipo_comprobantes,"productos"=>$productos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompraFormRequest $request)
    {
         try{
         DB::beginTransaction();
         $compra=new Compra;
            $compra->cod_proveedor=$request->get('cod_proveedor');
            $compra->cod_tipo_comprobante=$request->get('cod_tipo_comprobante');
            $compra->fecha_compra=$request->get('fecha_compra');
            $compra->numero_comprobante=$request->get('numero_comprobante');
            $compra->total_compra=$request->get('total_compra');
            $compra->estado='Activo';

            $compra->save();
             $cod_producto=$request->get('idarticulo');
            $cantidad=$request->get('cantidad');
            $precio_compra=$request->get('precio_compra');
            $descuento=$request->get('descuento');
            $cont=0;
            while ($cont<count($cod_producto)){
                $detalle=new DetalleCompra();
                $detalle->cod_compra=$compra->cod_compra;
                $detalle->cod_producto=$cod_producto[$cont];
                $detalle->cantidad=$cantidad[$cont];
                $detalle->precio_compra=$precio_compra[$cont];
                $detalle->descuento=$descuento[$cont];
                $detalle->save();
                $cont=$cont+1;

            }
            //actualizamos la cantidad en productos
            DB::Commit();
         }catch(\Exception $e)
        {
             DB::rollback();
        }
        return Redirect('compras')->with('info','Compra Guardada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function show(Compra $compra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function edit(Compra $compra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compra $compra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $compra=Compra::findOrFail($id);
        $compra->Estado='Anulado';
        $compra->update();
        return Redirect('compras')->with('info','Compra Anulada con exito');
    }
}
