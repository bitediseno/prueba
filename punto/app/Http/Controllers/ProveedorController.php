<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProveedorFormRequest;
use App\Proveedor;
use Illuminate\Http\Request;
use DB;
class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
      /* $this->middleware('permission:proveedores.create')->only(['create','store']);
        $this->middleware('permission:proveedores.index')->only(['index']);
        $this->middleware('permission:proveedores.edit')->only(['edit','update']);
        $this->middleware('permission:proveedores.show')->only(['show']);
        $this->middleware('permission:proveedores.destroy')->only(['destroy']);*/
    }
    public function index(Request $request)
    {
         if ($request)
        {
       /* $query=trim($request->get('searchText'));
        $proveedores=DB::table('clientes')
        ->where('cod_tipo_cliente','=','2')
        ->where('nombres','LIKE','%'.$query.'%')
         ->orwhere ('ruc','LIKE','%'.$query.'%')
         ->orwhere('cod_tipo_cliente','=','2')
        ->orderBy('cod_cliente', 'desc')
        ->paginate(7);*/
     $query=trim($request->get('searchText'));
        $proveedores=DB::table('clientes')
        ->where('cod_tipo_cliente','=','2')
        ->where('nombres','LIKE','%'.$query.'%')
        ->orderBy('cod_cliente', 'desc')
        ->paginate(7);

       
       
        return view('proveedores.index',["proveedores"=>$proveedores,"searchText"=>$query]);
    }
 }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('proveedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProveedorFormRequest $request)
    {
        $proveedor=new Proveedor;
        $proveedor->nombres=$request->get('razon_social');
        $proveedor->ruc=$request->get('ruc');
        $proveedor->correo=$request->get('email');
        $proveedor->telefono=$request->get('telefono');
        $proveedor->cod_tipo_cliente=2;
        $proveedor->save();
        /* $proveedor=Proveedor::create($request->all());*/
        return redirect('proveedores')->with('info','Proveedor Guardado con exito');
       // return view('proveedores.create')->with('info','Proveedor Guardado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd($id);
        return view("proveedores.show",["proveedor"=>Proveedor::findOrFail($id)]);
        //return view('proveedores.show',compact('proveedor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("proveedores.edit",["proveedor"=>Proveedor::findOrFail($id)]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(ProveedorFormRequest $request,  $id)
    {
        // $proveedor=Proveedor::create($request->all());
        //return redirect('proveedores')->with('info','Proveedor Guardado con exito');
       /*  $proveedor=Proveedor::findOrFail($id);
        //$proveedor->($request->all());
        $proveedor->update($request->all());
        return redirect('proveedores.edit',$id)->with('info','Proveedor actualizado con exito');
*/
         $proveedor=Proveedor::findOrFail($id);
       $proveedor->nombres=$request->get('razon_social');
        $proveedor->ruc=$request->get('ruc');
        $proveedor->correo=$request->get('email');
        $proveedor->telefono=$request->get('telefono');
        $proveedor->update();
        return redirect('proveedores')->with('info','Proveedor actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $proveedor=Proveedor::findOrFail($id);
        $proveedor->delete();
        return redirect('proveedores');
    }
}
