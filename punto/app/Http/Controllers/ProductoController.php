<?php

namespace App\Http\Controllers;
use App\Http\Requests\ProductoFormRequest;
use App\Producto;
use Illuminate\Http\Request;
use DB;
class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $query=trim($request->get('searchText'));
            $productos=DB::table('productos as p')
            ->join('marcas as m','m.cod_marca','=','p.cod_marca')
            ->join('categorias as c','c.cod_Categoria','=','p.cod_categoria')
            ->select('p.cod_producto','p.codigo','p.nombre_producto','m.desc_marca','c.desc_categoria','p.cantidad','p.precio_venta','p.estado','p.imagen')
            ->where('p.nombre_producto', 'LIKE','%'.$query.'%')
            ->orderBy('p.cod_producto','desc')
            ->paginate(7);
            return view('productos.index',["productos"=>$productos,"searchText"=>$query]);
    }

    
    public function create()
    {
         $marcas=DB::table('marcas')->where('estado','=','Activo')->get();
        $categorias=DB::table('categorias')->where('estado','=','Activo')->get();
            return view("productos.create",["marcas"=>$marcas,"categorias"=>$categorias]);
    }

    
    public function store(ProductoFormRequest $request)
    {
         $producto=new Producto;
        $producto->cod_marca=$request->get('cod_marca');
        $producto->cod_categoria=$request->get('cod_categoria');
        $producto->codigo=$request->get('codigo');
        $producto->nombre_producto=$request->get('nombre_producto');
         $producto->desc_producto=$request->get('desc_producto');
         $producto->cantidad=$request->get('cantidad');
         $producto->precio_compra=$request->get('precio_compra');
         $producto->precio_venta=$request->get('precio_venta');
        $producto->estado='Activo';
        $producto->porcentaje_impuesto='10';
       if($request->file('imagen')){
            $file=$request->file('imagen');
            $public_path=public_path();
            //public_patch().
            $file->move($public_path.'/imagenes/articulos/',$file->getClientOriginalName());
            $producto->imagen=$file->getClientOriginalName();
        }
        $producto->save();
        return redirect('productos');
    }

    
    public function show($id)
    {
         return view("productos.show",["producto"=>Producto::findOrFail($id)]);
    }

    
    public function edit($id)
    {
       $marcas=DB::table('marcas')->where('estado','=','Activo')->get();
        $categorias=DB::table('categorias')->where('estado','=','Activo')->get();
         $producto=Producto::findOrFail($id);
         return view("productos.edit",["producto"=>$producto,"categorias"=>$categorias,"marcas"=>$marcas]);
    }

    
    public function update(ProductoFormRequest $request,  $id)
    {
         $producto=Producto::findOrFail($id);;
        $producto->cod_marca=$request->get('cod_marca');
        $producto->cod_categoria=$request->get('cod_categoria');
        $producto->codigo=$request->get('codigo');
        $producto->nombre_producto=$request->get('nombre_producto');
         $producto->desc_producto=$request->get('desc_producto');
         $producto->cantidad=$request->get('cantidad');
         $producto->precio_compra=$request->get('precio_compra');
         $producto->precio_venta=$request->get('precio_venta');
        $producto->estado='Activo';
        $producto->porcentaje_impuesto='10';
       if($request->file('imagen')){
            $file=$request->file('imagen');
            $public_path=public_path();
            //public_patch().
            $file->move($public_path.'/imagenes/articulos/',$file->getClientOriginalName());
            $producto->imagen=$file->getClientOriginalName();
        }
        $producto->update();
        return redirect('productos');
    }

    
    public function destroy( $id)
    {
 $productos=Producto::findOrFail($id);
        $producto->estado='Inactivo';
        $producto->update();
        return redirect('productos');
    }

     function export_pdf($id) 
  { 
    //dd();
    // Recuperar todos los clientes de la base de datos 
    $data=DB::table('productos')->where('cod_producto',$id)->get(); 

    // Enviar datos a la vista utilizando la función loadView de la fachada PDF 
    $pdf=\PDF::loadView('productos.pdf.producto', compact('data')); 
    // Si desea almacenar el pdf generado en el servidor, puede usar la función de almacenamiento 
    //$pdf->save(storage_path().'_filename.pdf '); 
    // Finalmente, puede descargar el archivo usando la función de descarga 
    return $pdf->download('producto.pdf'); 
    
  } 

 
}
