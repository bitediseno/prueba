<?php

namespace App\Http\Controllers;
use App\Http\Requests\CategoraFormRequest;
use App\Categoria;
use Illuminate\Http\Request;
use DB;
class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
        $query=trim($request->get('searchText'));
        $categorias=DB::table('categorias')
        ->where('desc_categoria','LIKE','%'.$query.'%')
        ->orderBy('cod_categoria', 'desc')
        ->paginate(7);
        return view('categorias.index',["categorias"=>$categorias,"searchText"=>$query]);
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoraFormRequest $request)
    {
        $categoria=new Categoria;
        $categoria->desc_categoria=$request->get('desc_categoria');
        $categoria->estado='Activo';
        $categoria->save();
         return redirect('categorias')->with('info','Categoria Guardada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("categorias.edit",["categoria"=>Categoria::findOrFail($id)]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(CategoraFormRequest $request, $id)
    {
         $categoria=Categoria::findOrFail($id);
       $categoria->desc_categoria=$request->get('desc_categoria');
        $categoria->update();
        return redirect('categorias')->with('info','Categoria actualizada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
         $categoria=Categoria::findOrFail($id);
      $categoria->estado='Inactivo';
        $categoria->update();
        return redirect('categorias');
    }
}
