<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class InfProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
       $marcas=DB::table('marcas')->where('estado','=','Activo')->get();
        $categorias=DB::table('categorias')->where('estado','=','Activo')->get();
            return view("informes.productos.index",["marcas"=>$marcas,"categorias"=>$categorias]);
    }

    function todos_productos() 
  { 
    
    // Recuperar todos los clientes de la base de datos 
    $data=DB::table('productos')->get(); 

    // Enviar datos a la vista utilizando la función loadView de la fachada PDF 
   $pdf=\PDF::loadView('informes.productos.todos_pdf', compact('data')); 
   
    // Finalmente, puede descargar el archivo usando la función de descarga 
   return $pdf->download('todos_productos.pdf'); 
   
  } 
}
