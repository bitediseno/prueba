<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVenta extends Model
{
    protected $table='detalle_ventas';
    protected $primaryKey='cod_det_venta';
    public $timestamps=false;
    protected $fillable=['cod_venta','cod_producto','precio_venta','descuento',
'cantidad','subtotal'];
    protected $guarded=[];
}
