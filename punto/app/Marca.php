<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = 'marcas';
	public $timestamps=false;
    protected $primaryKey = 'cod_marca';
    protected $fillable = [
        'desc_marca',
    ];
}
