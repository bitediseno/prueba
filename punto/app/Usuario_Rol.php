<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario_Rol extends Model
{
    protected $table = 'role_user';
	public $timestamps=true;
    protected $primaryKey = 'id';
    protected $fillable = [
        'role_id','user_id',
    ];
}
