<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $table='compras';
    protected $primaryKey='cod_compra';
    public $timestamps=true;
    protected $fillable=['cod_compra','cod_proveedor','cod_tipo_comprobante','num_comprobante',
'total_compra','estado'];
    protected $guarded=[];
}
