@extends('layouts.principal')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">ROLES
                	

					@can('roles.create')
                	<a href="{{route('roles.create')}}" class="btn btn-sm btn-primary pull-right"> Crear</a>
                	@endcan
                	</div>
                <div class="panel-body">
                   <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Nombre</th>   .
                   		      <th colspan="3"></th>          		
                   		                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($roles as $role)
                   		<tr>
                   			<td>{{$role->id}}</td>
                   			<td>{{$role->name}}</td>
                   			<td>
                   				@can('roles.show')
                   				<a href="{{route('roles.show',$role->id)}}"><span style="font-size: 20px;"><i class="fa fa-info"></i></span></a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('roles.edit')
                   				<a href="{{route('roles.edit',$role->id)}}" ><span style="font-size: 20px;"><i class="fa fa-edit"></i></span></a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('roles.destroy')
                   				<form action="{{url('roles',[$role->id])}}" method="POST">
								{{method_field('delete')}}
								{{csrf_field()}}
                   					
                   					<span style="font-size: 20px;"><i class="fa fa-trash"></i></span>
                   				</form>
                   				
                   				@endcan
                   			</td>
                   		</tr>
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$roles->render()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
