@extends('layouts.principal')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Roles
                	
                	</div>
                <div class="panel-body">
                	<form action="{{url('roles')}}" method="POST" enctype="multipart/form-data">
				
				{{csrf_field()}}
                		
                		<div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" required  class="form-control" placeholder="Nombre...">
        </div>
        <div class="form-group">
                    <label for="name">Slug</label>
                    <input type="text" name="slug" required  class="form-control" placeholder="URL amigable">
        </div>
        <div class="form-group">
                    <label for="nombre">Descripcion</label>
                    <input type="text" name="description" required  class="form-control" placeholder="Descripcion..">
        </div>

        <hr>
        <h3>Permiso especial</h3>
        <div class="form-group">
            
            <input type="radio"  name="special" value="All-access">
            <label for="acceso_total">acceso_total</label>
            <input type="radio"  name="special" value="no-access">
            <label for="ningun_acceso">ningun_acceso</label>
        </div>
        <hr>
        <h3>Lista de Permisos</h3>

        <ul class="list-unstyled">
                @foreach($permissions as $permission)
                
                    <li>
                        <input type="checkbox"  name="permissions[]" value="{{$permission->id}}">
                        {{$permission->name}}  <em>{{$permission->description ?:'N/A'}}</em>
                    </li>
                
                @endforeach
            </ul>
        <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection