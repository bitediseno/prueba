@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Ventas</span>
                        
                        <a href="{{url('ventas/create')}}" Class="pull-right">
                              
                              <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nueva Venta
                              </span>
                         </a>
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>@include('ventas.search')
			<div class="col-md-12 ">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      
                                    <th >Fecha</th>
                                     <th >Cliente</th>
                                     <th >Tipo Venta</th> 
                                    <th >Cantidad</th>  
                                    <th >Numero Factura</th> 
                                    <th >Total</th>
                                    <th >Estado</th>
                   		      <th colspan="4"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($ventas as $venta)
                   		<tr>
                   			<td>{{$venta->cod_venta}}</td>

                   			<td>{{$venta->fecha_venta}}</td>
                                    <td>{{$venta->nombres}}</td>
                                    <td>{{$venta->desc_tipo_venta}}</td>
                                    <td>{{$venta->cantidad}}</td>
                                    <td>{{$venta->numero_factura}}</td>
                                    <td>{{$venta->total_venta}}</td>
                                    <td>{{$venta->estado}}</td>
                   			<td>
                   				
                   				<a href="{{url('ventas',$venta->cod_venta)}}"  name="venta"><span style="font-size: 20px;"><i class="fa fa-info"></i></span></a>
                   				
                   			</td>
                                    <td>
                   				
                   				<a href="#" ><span style="font-size: 20px;"><i class="fa fa-edit"></i></span></a>
                   				
                                    </td>
                                    <td>
                   			                				
                   				<a href="" data-target="#modal-delete-{{$venta->cod_venta}}" data-toggle="modal" >
                                                <span style="font-size: 20px; "><i class="fa fa-trash"></i></span></a>
                   				
                                    </td>
                                     <td>
                                          
                                          <a href="{{route('factura_pdf',['id'=> $venta->cod_venta])}}"target="_blank"><span style="font-size: 20px; "><i class="fa fa-file-pdf-o"></i></span></a>
                                          
                                          
                                    </td>
                   			
                   		</tr>
                              @include('ventas.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$ventas->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection