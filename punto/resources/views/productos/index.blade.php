@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Productos</span>
                        @can('productos.create')
                        <a href="{{url('productos/create')}}" Class="pull-right">
                              
                              <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nuevo Producto
                              </span>
                         </a>
                         @endcan
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>@include('productos.search')
			<div class="col-md-12 ">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Codigo</th> 
                                    <th >Descripcion</th>  
                                    <th >Marca</th>  
                                    <th >Categoria</th> 
                                    <th >Cantidad</th> 
                                    <th >Precio Venta</th>
                                    <th >Imagen</th>
                                    <th >Estado</th>
                   		      <th colspan="4"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($productos as $producto)
                   		<tr>
                   			<td>{{$producto->cod_producto}}</td>
                   			<td>{{$producto->codigo}}</td>
                                    <td>{{$producto->nombre_producto}}</td>
                                    <td>{{$producto->desc_marca}}</td>
                                    <td>{{$producto->desc_categoria}}</td>
                                    <td>{{$producto->cantidad}}</td>
                                    <td>{{$producto->precio_venta}}</td>
                                    <td><img src="{{url('imagenes/articulos/'.$producto->imagen)}}" alt="{{$producto->nombre_producto}}" height="50px" width="50px" class="img-thumbnail">
                                    </td>
                                    <td>{{$producto->estado}}</td>
                   			<td>
                   				@can('productos.show')
                   				<a href="{{url('productos',$producto->cod_producto)}}"  name="producto"><span style="font-size: 20px;"><i class="fa fa-info"></i></span></a>
                   				@endcan
                   			</td>
                                    <td>
                   				@can('productos.edit')
                   				<a href="{{action('ProductoController@edit',$producto->cod_producto)}}" ><span style="font-size: 20px;"><i class="fa fa-edit"></i></span></a>
                   				@endcan
                                    </td>
                                    <td>
                   			     @can('productos.destroy')           				
                   				<a href="" data-target="#modal-delete-{{$producto->cod_producto}}" data-toggle="modal" >
                                                <span style="font-size: 20px; "><i class="fa fa-trash"></i></span></a>
                   			  @endcan
                                    </td>
                   			
                   		</tr>
                              @include('productos.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$productos->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection