@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
        <a href="{{url('productos')}}">Productos</a>
        <i class="fa fa-angle-right"></i>
				<span>Modificar Producto</span>
                        
			</h2>

		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
			<div class="col-md-12 ">
			<div class="content-top-1">
				@if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{$error}}</li>
                              @endforeach

                        </ul>

                  </div>
                  @endif
				 <form action="{{url('productos',[$producto->cod_producto])}}" method="POST" enctype="multipart/form-data">
				{{method_field('PATCH')}}
				{{csrf_field()}}
                		
                		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                    <label for="name">Codigo</label>
                    <input type="text" name="codigo" required  class="form-control"  value="{{$producto->codigo}}">
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre_producto" required  class="form-control" placeholder="Nombre.." value="{{$producto->nombre_producto}}">
                    </div>    
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                    <label for="marca">Marca</label>
                    <select name="cod_marca" id="cod_marca" class="form-control selectpicker" data-live-search="true">
                      @foreach($marcas as $marca)
                      @if($producto->cod_marca==$marca->cod_marca)
                      <option value="{{$marca->cod_marca}}" selected>{{$marca->desc_marca}} </option>
                      @else
                        <option value="{{$marca->cod_marca}}">{{$marca->desc_marca}} </option>
                      @endif
                      @endforeach
                    </select>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                    <label for="categoria">Categoria</label>
                    <select name="cod_categoria" id="cod_categoria" class="form-control selectpicker" data-live-search="true">
                      @foreach($categorias as $categoria)
                      @if ($categoria->cod_categoria)
                      <option value="{{$categoria->cod_categoria}}" selected>{{$categoria->desc_categoria}} </option>
                      @else
                      <option value="{{$categoria->cod_categoria}}" >{{$categoria->desc_categoria}} </option>
                       @endif
                      @endforeach
                    </select>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
                    <label for="desc_producto">Descripcion</label>
                    <input type="text" name="desc_producto"   class="form-control" vale="{{$producto->desc_producto}}">
        </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
         <div class="form-group">
                    <label for="cantidad">Cantidad</label>
                    <input type="text" name="cantidad" required  class="form-control" value="{{$producto->cantidad}}" >
        </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
         <div class="form-group">
                    <label for="precio_compra">Precio Compra</label>
                    <input type="text" name="precio_compra" required  class="form-control" value="{{$producto->precio_compra}}">
        </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
                    <label for="precio_venta">Precio Venta</label>
                    <input type="text" name="precio_venta" required  class="form-control" value="{{$producto->precio_venta}}">
        </div>
      </div>
      <div class="form-group">
          <label for="imagen">Imagen</label>
          <input type="file" name="imagen"  class="form-control">
          @if (($producto->imagen)!="")
            <img src="{{url('imagenes/articulos/'.$producto->imagen)}}" height="100px" width="100px">
          @endif
          </div>
        <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
                	</form>
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection
