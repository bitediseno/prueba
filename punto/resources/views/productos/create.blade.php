@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
        <a href="{{url('productos')}}">Productos</a>
        <i class="fa fa-angle-right"></i>
				<span>Nuevo Producto</span>
                        
			</h2>

		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
			<div class="col-md-12 ">
			<div class="content-top-1">
				@if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{$error}}</li>
                              @endforeach

                        </ul>

                  </div>
                  @endif
				 <form action="{{url('productos')}}" method="POST" enctype="multipart/form-data">
				
				{{csrf_field()}}
                	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                		<div class="form-group">
                    <label for="name">Codigo</label>
                    <input type="text" name="codigo" required  class="form-control" placeholder="Codigo de Producto.." value="{{old('nombre_producto')}}">
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre_producto" required  class="form-control" placeholder="Nombre.." value="{{old('nombre_producto')}}">
                    </div>    
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                    <label for="marca">Marca</label>
                    <select name="cod_marca" id="cod_marca" class="form-control selectpicker" data-live-search="true">
                      @foreach($marcas as $marca)
                      <option value="{{$marca->cod_marca}}">{{$marca->desc_marca}} </option>
                      @endforeach
                    </select>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <div class="form-group">
                    <label for="categoria">Categoria</label>
                    <select name="cod_categoria" id="cod_categoria" class="form-control selectpicker" data-live-search="true">
                      @foreach($categorias as $categoria)
                      <option value="{{$categoria->cod_categoria}}">{{$categoria->desc_categoria}} </option>
                      @endforeach
                    </select>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
                    <label for="desc_producto">Descripcion</label>
                    <input type="text" name="desc_producto"   class="form-control" placeholder="Descripcion del Producto">
        </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
         <div class="form-group">
                    <label for="cantidad">Cantidad</label>
                    <input type="text" name="cantidad" required  class="form-control" placeholder="Cantidad..">
        </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
         <div class="form-group">
                    <label for="precio_compra">Precio Compra</label>
                    <input type="text" name="precio_compra" required  class="form-control" placeholder="Precio de compra..">
        </div>
      </div>
      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
        <div class="form-group">
                    <label for="precio_venta">Precio Venta</label>
                    <input type="text" name="precio_venta" required  class="form-control" placeholder="Precio de venta..">
        </div>
      </div>
      <div class="form-group">
          <label for="imagen">Imagen</label>
          <input type="file" name="imagen"  class="form-control">
          </div>
        <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
                	</form>
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection
