@extends('layouts.principal')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Informe de Productos
                	</div>
                <div class="panel-body">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                  <div class="label label-success">Todos los productos</div>
                  <a href="{{route('todos_pdf')}}"target="_blank"><span style="font-size: 20px; "><i class="fa fa-file-pdf-o"></i></span></a>
              </div>
               <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                  <div class="label label-success">Productos por Categoria </div>

              </div>
                  		<form action="#" method="POST">
								{{method_field('delete')}}
								{{csrf_field()}}
                   		 <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
		                    <div class="form-group">
		                    <label for="categoria">Categoria</label>
		                    <select name="cod_categoria" id="cod_categoria" class="form-control selectpicker" data-live-search="true">
		                      @foreach($categorias as $categoria)
		                      <option value="{{$categoria->cod_categoria}}">{{$categoria->desc_categoria}} </option>
		                      @endforeach
		                    </select>
		                    </div>
                  		</div>
                  		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">	
                  		<div class="form-group">
		                    <button class="btn btn-primary" type="submit">Mostrar</button>
		                   
		                </div>		
		            </div>
                   		</form>

                 <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                   <div class="label label-success">10 Productos mas Vendidos</div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection