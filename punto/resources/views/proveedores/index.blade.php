@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Proveedores</span>
                        @can('proveedores.create')
                        <a href="{{url('proveedores/create')}}" class="btn btn-sm  pull-right">
                         <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nuevo Proveedor
                              </span></a>
                         @endcan
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->

 	<div class="content-top">

            </div>@include('proveedores.search')
			<div class="col-md-12 ">
			<div class="content-top-1">
                        
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Nombre</th>  
                                    <th >Ruc</th>  
                                    <th >Telefono</th> 
                                    <th >Correo</th> 
                   		      <th colspan="3"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($proveedores as $proveedor)
                   		<tr>
                   			<td>{{$proveedor->cod_cliente}}</td>
                   			<td>{{$proveedor->nombres}}</td>
                                    <td>{{$proveedor->ruc}}</td>
                                    <td>{{$proveedor->telefono}}</td>
                                    <td>{{$proveedor->correo}}</td>
                   			<td>
                   				@can('proveedores.show')
                   				<a href="{{url('proveedores',$proveedor->cod_cliente)}}"  name="proveedor"><span style="font-size: 20px;"><i class="fa fa-info"></i></span></a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('proveedores.edit')
                   				<a href="{{action('ProveedorController@edit',$proveedor->cod_cliente)}}" ><span style="font-size: 20px;"><i class="fa fa-edit"></i></span></a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('proveedores.destroy')
                   				<a href="" data-target="#modal-delete-{{$proveedor->cod_cliente}}" data-toggle="modal"><span style="font-size: 20px;"><i class="fa fa-trash"></i></span></a>
                   				@endcan
                   			</td>
                   		</tr>
                              @include('proveedores.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$proveedores->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection