@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Marcas Productos</span>
                        @can('marcas.create')
                        <a href="{{url('marcas/create')}}" class="btn btn-sm  pull-right">
                         <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nueva Marca
                              </span></a>
                        @endcan
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>@include('marcas.search')
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Desc_Marca</th>  
                                    <th >Estado</th> 
                   		      <th colspan="3"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($marcas as $marca)
                   		<tr>
                   			<td>{{$marca->cod_marca}}</td>
                   			<td>{{$marca->desc_marca}}</td>
                                    <td>{{$marca->estado}}</td>
                   			
                   			<td>
                   				@can('marcas.edit')
                   				<a href="{{action('MarcaController@edit',$marca->cod_marca)}}" class="btn btn-xs">Editar</a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('marcas.destroy')
                   				<a href="" data-target="#modal-delete-{{$marca->cod_marca}}" data-toggle="modal"><button class="btn btn-xs btn-danger">Eliminar</button></a>
                   				@endcan
                   			</td>
                   		</tr>
                              @include('marcas.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$marcas->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection