
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML >
<html lang="{{ app()->getLocale() }}">
<head>
<title>Sistema Punto de Venta</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
 <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-select.min.css')}}"/>
<!-- Custom Theme files -->
<link href="{{asset('bootstrap/css/style.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('bootstrap/css/font-awesome.css')}}" rel="stylesheet"> 
<!-- Mainly scripts -->
<script src="{{asset('bootstrap/js/jquery.metisMenu.js')}}"></script>
<script src="{{asset('bootstrap/js/jquery.slimscroll.min.js')}}"></script>
<link href="{{asset('bootstrap/css/custom.css')}}" rel="stylesheet">

		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});
			

			
		});
		</script>



</head>
<body>
<div id="wrapper">
       <!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" >Ashur</a></h1>         
			   </div>
			 <div class=" border-bottom">
        	<div class="full-left">
        	  <section class="full-top">
				<button id="toggle"><i class="fa fa-arrows-alt"></i></button>	
			</section>
			 @if(session('info'))
            
                <div class="row">
        
                <div class="col-sd-8 col-sd-offset-2">
            <div class="alert alert-success">
                {{session('info')}}
            </div>
        </div>
    </div>


@endif
            <div class="clearfix"> </div>
           </div>
     
       
            <!-- Brand and toggle get grouped for better mobile display -->
		 
		   <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="drop-men" >
		        <ul class=" nav_1">
		           
					<li class="dropdown">
		              <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">{{ Auth::user()->name }}<i class="caret"></i></span><img src="{{asset('imagenes/logo.png')}}" width="50px" height="50px"></a>
		              <ul class="dropdown-menu " role="menu">
		                
		      
		                
		                <li><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fa fa-clipboard"></i>Salir</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form></li>
		              </ul>
		            </li>
		           
		        </ul>
		     </div><!-- /.navbar-collapse -->
			<div class="clearfix">
       
     </div>
	  
		    <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
				    @can('dashboard')
                    <li>
                        <a href="{{url('/')}}" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>
                    </li>
                   @endcan
                    @can('compras')
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Compras</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('compras')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Ingreso Compras</a></li>
                            <li><a href="{{url('productos')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Productos</a></li>
                            <li><a href="{{url('categorias')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Categorias</a></li>
                             <li><a href="{{url('marcas')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Marcas</a></li>
                            <li><a href="{{url('proveedores')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Proveedores</a></li>
    					</ul>
                    </li>
                    @endcan
                     @can('venta')
					<li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Ventas</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                             @can('venta')
                            <li><a href="{{url('ventas')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Ingreso Ventas</a></li>
                            @endcan
                            @can('clientes.index')
                            <li><a href="{{route('clientes.index')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Clientes</a></li>
                           @endcan
    					</ul>
                    </li>
                    @endcan
                     @can('usuarios')
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Usuarios</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('usuarios')}}" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Usuarios</a></li>
                            <li><a href="{{url('roles')}}" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Roles</a></li>
                        </ul>
                    </li>
                    @endcan
                    <li>
                        <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-info nav_icon"></i> <span class="nav-label">Informes</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{url('informes/productos')}}" class=" hvr-bounce-to-right"><i class="fa fa-info nav_icon"></i>Productos</a></li>
                            <li><a href="{{url('informes/ventas')}}" class=" hvr-bounce-to-right"><i class="fa fa-info nav_icon"></i>Ventas</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
			</div>
        </nav>

       
        @yield('content')
			<!--//faq-->
		<!---->
<div class="copy">
            <p> &copy; 2020 Bite - Software. All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>	    
        </div>
		</div>
		</div>

		<div class="clearfix"> </div>
 </div>
     
<!---->
<!--scrolling js-->
<script src="{{asset('bootstrap/js/jquery.min.js')}}"> </script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"> </script>
   <script src="{{asset('bootstrap/js/bootstrap-select.min.js')}}"></script>

<!-- Custom and plugin javascript 

<script src="{{asset('bootstrap/js/custom.js')}}"></script>
<script src="{{asset('bootstrap/js/screenfull.js')}}"></script>
	<script src="{{asset('bootstrap/js/jquery.nicescroll.js')}}"></script>
	<script src="{{asset('bootstrap/js/scripts.js')}}"></script>-->
	<!--//scrolling js

 <script src="{{asset('bootstrap/js/jquery.min.js')}}"> </script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"> </script>-->
  

<script src="{{asset('bootstrap/js/jquery.metisMenu.js')}}"></script>
<!--<script src="{{asset('bootstrap/js/jquery.slimscroll.min.js')}}"></script>-->
<!-- Custom and plugin javascript 
<link href="{{asset('bootstrap/css/custom.css" rel="stylesheet')}}">-->
<script src="{{asset('bootstrap/js/custom.js')}}"></script>
<script src="{{asset('bootstrap/js/screenfull.js')}}"></script>
        
<!---->


    @stack('scripts')
</body>
</html>


