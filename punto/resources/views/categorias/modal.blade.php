<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$categoria->cod_categoria}}">
	
<form action="{{url('categorias',[$categoria->cod_categoria])}}" method="POST">
	{{method_field('DELETE')}}
				{{csrf_field()}}
	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
						<span aria-hidden="true">x</span>

					</button>
					<h4 class="modal-tittle">Eliminar Marca</h4>
				</div>
				<div class="modal-body">
					<p>Confirme si desea eliminar la Marca
					<span class="label label-warning">{{$categoria->desc_categoria}}</span></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Confirmar</button>
				</div>

			</div>


</form>

</div>