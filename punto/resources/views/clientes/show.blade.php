@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Clientes</span>
              
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>
			<div class="col-md-12 ">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Nombre</th>  
                                    <th >Ruc</th>  
                                    <th >Telefono</th> 
                                    <th >Correo</th> 
                   		      <th colspan="3"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   	
                   		<tr>
                   			<td>{{$cliente->cod_cliente}}</td>
                   			<td>{{$cliente->nombres}}</td>
                            <td>{{$cliente->ruc}}</td>
                            <td>{{$cliente->telefono}}</td>
                            <td>{{$cliente->correo}}</td>
                   			
                   		</tr>
                   	
                   	</tbody>
                   </table>
                   
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection