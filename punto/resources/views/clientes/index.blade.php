@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Clientes</span>
                                            
                      @can('clientes.create')
                        <a href="{{url('clientes/create')}}" class="btn btn-sm  pull-right">
                         <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nuevo Cliente
                              </span></a>
                        @endcan
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>@include('clientes.search')
			<div class="col-md-12 ">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Nombre</th>  
                                    <th >Ruc</th>  
                                    <th >Telefono</th> 
                                    <th >Correo</th> 
                   		      <th colspan="3"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($clientes as $cliente)
                   		<tr>
                   			<td>{{$cliente->cod_cliente}}</td>
                   			<td>{{$cliente->nombres}}</td>
                                    <td>{{$cliente->ruc}}</td>
                                    <td>{{$cliente->telefono}}</td>
                                    <td>{{$cliente->correo}}</td>
                   			<td>
                   				@can('clientes.show')
                   				<a href="{{url('clientes',$cliente->cod_cliente)}}"  name="cliente"><span style="font-size: 20px;"><i class="fa fa-info"></i></span></a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('clientes.edit')
                   				<a href="{{action('ClienteController@edit',$cliente->cod_cliente)}}" ><span style="font-size: 20px;"><i class="fa fa-edit"></i></span></a>
                   				@endcan
                   			</td>
                   			<td>
                   				@can('clientes.destroy')
                   				<a href="" data-target="#modal-delete-{{$cliente->cod_cliente}}" data-toggle="modal"><span style="font-size: 20px;"><i class="fa fa-trash"></i></span></a>
                   				@endcan
                   			</td>
                   		</tr>
                              @include('clientes.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$clientes->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection