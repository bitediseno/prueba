@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Nuevo Usuario</span>
                        
			</h2>

		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
			<div class="col-md-12 ">
			<div class="content-top-1">
				@if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{$error}}</li>
                              @endforeach

                        </ul>

                  </div>
                  @endif
				 <form action="{{url('usuarios')}}" method="POST" enctype="multipart/form-data">
				
				{{csrf_field()}}
                		
                		<div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" required  class="form-control" placeholder="Nombre usuario">
        			</div>
        				<div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" name="email" required  class="form-control" placeholder="Correo">
        			</div>
        				<div class="form-group">
                    <label for="name">Password</label>
                    <input type="text" name="password" required  class="form-control" placeholder="Contraseña">
        			</div>
              <div class="form-group">
                    <label for="perfil">Perfil</label>
                    <select name="perfil" id="perfil" class="form-control selectpicker">
                      @foreach($perfiles as $perfil)
                      <option value="{{$perfil->id}}">{{$perfil->name}} </option>
                      @endforeach
                    </select>
                    </div>
       
        <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
                	</form>
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection
