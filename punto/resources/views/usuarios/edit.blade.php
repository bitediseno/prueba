@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
 
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Modificar Usuario</span>
                        
			</h2>

		    </div>
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
			<div class="col-md-12 ">
			<div class="content-top-1">
				@if ($errors->any())
                  <div class="alert alert-danger">
                        <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{$error}}</li>
                              @endforeach

                        </ul>

                  </div>
                  @endif
				 <form action="{{url('usuarios',[$usuario->id])}}" method="POST" enctype="multipart/form-data">
				{{method_field('PATCH')}}
				{{csrf_field()}}
                		

                		<div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" required  class="form-control" value="{{$usuario->name}}">
        			</div>
        				<div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" name="email" required  class="form-control" value="{{$usuario->email}}">
        			</div>
        		
        
        <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
                	</form>
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection
