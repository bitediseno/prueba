@extends('layouts.principal')

@section('content')

 <div id="page-wrapper" class="gray-bg dashbard-1">
       <div class="content-main">
            
 	<!--banner-->	
		     <div class="banner">
		    	<h2>
				<a href="{{url('/')}}">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Usuarios</span>
                        
                        <a href="{{url('usuarios/create')}}" class="btn btn-sm  pull-right">
                         <span style="font-size: 14px;">
                              <i class="fa fa-plus"></i>Nuevo Usuario
                              </span></a>
			</h2>
 
		    
		<!--//banner-->
 	 <!--faq-->
 	<div class="content-top">
            </div>@include('usuarios.search')
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="content-top-1">
				 <table class="table table-striped table-hover">
                   	<thead>
                   		<tr>
                   			<th width="10px">ID</th>
                   		      <th >Nombre</th>  
                                    <th >Email</th> 
                   		      <th colspan="3"></th>          		
                   		</tr>

                   	</thead>
                   	<tbody>
                   		@foreach($usuarios as $usuario)
                   		<tr>
                   			<td>{{$usuario->id}}</td>
                   			<td>{{$usuario->name}}</td>
                                    <td>{{$usuario->email}}</td>
                   			
                   			<td>
                   				
                   				<a href="{{action('UsuarioController@edit',$usuario->id)}}" ><span style="font-size: 20px;"><i class="fa fa-edit"></i></span></a>
                   				
                   			</td>
                   			
                   		</tr>
                              @include('usuarios.modal')
                   		@endforeach
                   	</tbody>
                   </table>
                   {{$usuarios->render()}}
		</div>
	</div>
		<div class="clearfix"> </div>
	</div>
	

@endsection